import { Route, Switch } from 'react-router-dom';

import HomeComponent from './components/home/Home.jsx';
import MotherboardContainer from './components/pages/Containers/Motherboard.jsx';
import MemoryContainer from './components/pages/Containers/Memory.jsx';
import VideoCardContainer from './components/pages/Containers/VideoCard.jsx';
import FinishContainer from './components/pages/Containers/Finish.jsx';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={HomeComponent} />
    <Route exact path="/motherboard" component={MotherboardContainer} />
    <Route exact path="/memory" component={MemoryContainer} />
    <Route exact path="/video-card" component={VideoCardContainer} />
    <Route exact path="/finish" component={FinishContainer} />
  </Switch>
);

export default Routes;
