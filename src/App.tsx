import React from 'react';
import { withRouter } from 'react-router-dom';

import Header from './components/Header';

import './App.css';
import './assets/scss/main.scss';

type Props = {
  children: any,
  history: any,
  location: any,
  match: any,
  staticContext?: any,
};

function App(props: Props) {
  return (
    <div className="App">
      <Header location={props.location} />

      { props.children }
    </div>
  );
}

export default withRouter(App);
