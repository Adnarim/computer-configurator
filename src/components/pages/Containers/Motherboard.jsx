import { useState, useEffect } from 'react';

import dataObj from '../data';
import Navigator from '../Components/Navigator';
import ListCol from '../Components/ListCol';
import DetailsCol from '../Components/DetailsCol';
import Loading from '../Components/Loading';

const MotherboardContainer = () => {
  const [choice, setChoice] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const data = dataObj.motherboards;
  const storageName = 'motherboard';
  const navigator = {
    title: 'Select a motherboard',
    prev: '/',
    next: '/memory',
  };

  useEffect(() => {
    const init = () => {
      setIsLoading(true);

      const storedChoice = localStorage.getItem(storageName);
      if (!storedChoice) {
        // define first motherboard as option into store and state
        setChoice(data[0]);
      } else {
        // update state, we already have the data stored
        setChoice(JSON.parse(storedChoice));
      }

      setIsLoading(false);
    }

    init();
  }, [data]);

  /**
   * Update state and store with new choice
   **/
  const changeChoice = (newChoice) => {
    setChoice(newChoice);
    localStorage.setItem(storageName, JSON.stringify(newChoice));
  }

  /**
   * Render list of producs
   **/
  const renderList = () => {
    return (
      <ListCol
        onChange={(item) => changeChoice(item)}
        items={data}
        activeItem={choice.id}
        name="boards"
      />
    );
  }

  /**
   * Render right column with product details
   */
  const renderDetails = () => {
    return (
      <DetailsCol nextLink={navigator.next}>
        <div className="float-sm-right">
          <img src={choice.image} alt="product" />
        </div>
        <h2>{choice.name}</h2>
        <div><strong>Socket</strong>: {choice.socket}</div>
        <div><strong>Chipset</strong>: {choice.chipset}</div>
        <div><strong>Formfactor</strong>: {choice.formfactor}</div>
        <div><strong>Max memory</strong>: {choice.memory_max}</div>
        <div><strong>Memory slots</strong>: {choice.memory_slots}x {choice.memory_slots_type}</div>
        {choice.usbc_3_1 && <div><strong>USB-C 3.1</strong>: {choice.usbc_3_1}</div>}
        {choice.usb_3_1 && <div><strong>USB 3.1</strong>: {choice.usb_3_1}</div>}
        {choice.usb_3_0 && <div><strong>USB 3.0</strong>: {choice.usb_3_0}</div>}
        {choice.usb_2_0 && <div><strong>USB 2.0</strong>: {choice.usb_2_0}</div>}
        {choice.dvid && <div><strong>DVI-D</strong>: {choice.dvid}</div>}
        {choice.hdmi && <div><strong>HDMI</strong>: {choice.hdmi}</div>}
        {choice.mini_hdmi && <div><strong>Mini-HDMI</strong>: {choice.mini_hdmi}</div>}
        <div><strong>Line-in</strong>: {choice.linein}</div>
        <div><strong>Line-out</strong>: {choice.lineout}</div>
        {choice.rj45 && <div><strong>RJ-45</strong>: {choice.rj45}</div>}
        {choice.bluetooth && <div><strong>Bluetooth</strong>: {choice.bluetooth}</div>}
        {choice.wlan && <div><strong>WLAN</strong>: {choice.wlan}</div>}
        <div><strong>Sata</strong>: {choice.sata}</div>
      </DetailsCol>
    );
  }

  /**
   * Render content
   */
  const renderContent = () => {
    if (isLoading) return <Loading />

    return (
      <div className="row d-flex flex-grow-1">
        { renderList() }

        { renderDetails() }
      </div>
    );
  }

  /**
   * Render view
   **/
  return (
    <section className="AppContent d-flex justify-content-stretch align-items-stretch flex-column">
      <Navigator
        title={navigator.title}
        // prev={navigator.prev}
        // next={navigator.next}
      />

      <div className="container-fluid d-flex flex-grow-1 align-items-stretch">
        { renderContent() }
      </div>
    </section>
  );
}

export default MotherboardContainer;