import { useState, useEffect } from 'react';

import dataObj from '../data';
import Navigator from '../Components/Navigator';
import ListCol from '../Components/ListCol';
import DetailsCol from '../Components/DetailsCol';
import Notifier from '../Components/Notifier';
import Loading from '../Components/Loading';

const MemoryContainer = () => {
  const [choice, setChoice] = useState(null);
  const [hasPreviousStorage, setHasPreviousStorage] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const data = dataObj.memory_banks;
  const storageName = 'memory';
  const navigator = {
    title: 'Select a memory card',
    prev: '/motherboard',
    next: '/video-card',
  };

  useEffect(() => {
    const init = () => {
      setIsLoading(true);

      const hasMobo = !!localStorage.getItem('motherboard');
      const localStorageChoice = localStorage.getItem(storageName);
      
      setHasPreviousStorage(hasMobo);

      if (hasMobo) {
        if (!localStorageChoice) {
          // define first motherboard as option into store and state
            changeChoice(data[0]);
        } else {
          // update state, we already have the data stored
          setChoice(JSON.parse(localStorageChoice));
        }
      }


      const amountField = document.getElementById('amount');

      if (hasPreviousStorage) {
        const storedMemoryAmount = localStorage.getItem('memory_amount');

        if (storedMemoryAmount && amountField) {
          amountField.value = storedMemoryAmount;
        } else {
          localStorage.setItem('memory_amount', 1);
        }
      }

      setIsLoading(false);
    }

    init();
  }, [data, hasPreviousStorage]);

  /**
   * Update state and store with new choice
   **/
  const changeChoice = (newChoice) => {
    setChoice(newChoice);
    localStorage.setItem(storageName, JSON.stringify(newChoice));
  }

  /**
   * Render list of producs
   **/
  const renderList = () => {
    return (
      <ListCol
        onChange={(item) => changeChoice(item)}
        items={data}
        activeItem={choice.id}
        name="boards"
      />
    );
  }

  /**
   * Render right column with product details
   */
  const renderDetails = () => {
    const motherboard = JSON.parse(localStorage.getItem('motherboard'));

    return (
      <DetailsCol
        nextLink={navigator.next}
        canHaveMultiple={{
          'max_memory': motherboard.memory_max,
          'slots': motherboard.memory_slots,
          'choice_capacity': choice.capacity,
          'choice_pieces': choice.pieces
        }}
      >
        <div className="float-sm-right">
          <img src={choice.image} alt="product" />
        </div>
        <h2>{choice.name}</h2>
        <div><strong>Type</strong>: {choice.type}</div>
        <div><strong>Capacity</strong>: {choice.capacity}GB</div>
        <div><strong>Timings</strong>: {choice.timings}</div>
        <div><strong>Pieces</strong>: {choice.pieces}</div>
        <div><strong>Connector</strong>: {choice.connector}</div>
        <div><strong>Voltage</strong>: {choice.voltage}</div>
        <div><strong>Standard</strong>: {choice.standard}</div>
        <div><strong>Physical Clock Speed</strong>: {choice.physical_clock_speed}</div>
      </DetailsCol>
    );
  }

  /**
   * Render content
   */
  const renderContent = () => {
    if (isLoading) return <Loading />

    if (!hasPreviousStorage) {
      return (
        <Notifier notification="Please select a motherboard first" link={navigator.prev} />
      );
    }
    return (
      <div className="row d-flex flex-grow-1">
        { renderList() }
        { renderDetails() }
      </div>
    );
  }

  /**
   * Render view
   */
  return (
    <section className="AppContent d-flex justify-content-stretch align-items-stretch flex-column">
      <Navigator
        title={navigator.title}
        />

      <div className="container-fluid d-flex flex-grow-1 align-items-stretch">
        { renderContent() }
      </div>
    </section>
  );
}

export default MemoryContainer;