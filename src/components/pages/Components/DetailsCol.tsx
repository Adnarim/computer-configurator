import { useEffect } from 'react';
import { Link } from 'react-router-dom';

type Props = {
  canHaveMultiple?: {
    slots: string,
    max_memory: string,
    choice_capacity: string,
    choice_pieces: string,
  },
  nextLink: string,
  children: any,
};

const DetailsCol = (props: Props) => {
  useEffect(() => {
    const init = () => {
      /**
       * If choice changes, recheck if current amount is not exceeding the new max
       */
      if (props.canHaveMultiple) {
        console.error(props.canHaveMultiple)
        checkExceeding(generateMax());
      }
    };

    init();
  });

  
  /**
   * Check if the current amount is not exceeding the max
   * and store the chosen amount in the store
   */
  const checkExceeding = (max: number | undefined) => {
    if (max) {
      const amountEl = (document.getElementById('amount') as HTMLInputElement);
  
      if (amountEl) {
        const value = parseInt(amountEl.value);
    
        if (value > max) {
          amountEl.value = max.toString();
        }
    
        if (props.canHaveMultiple) {
          localStorage.setItem('memory_amount', amountEl.value);
        }
      }
    }
  }

  /**
   * Check what can be the max based on memory bank and motherboard specs
   */
  const generateMax = () => {
    if (props.canHaveMultiple) {
      const slots = parseInt(props.canHaveMultiple.slots);
      const max_memory = parseInt(props.canHaveMultiple.max_memory);
      const choice_capacity = parseInt(props.canHaveMultiple.choice_capacity);
      const choice_pieces = parseInt(props.canHaveMultiple.choice_pieces);
  
      let max = 1;
      if (slots >= choice_pieces) {
        if (max_memory % choice_capacity === 0) {
          max = max_memory / choice_capacity;
        } else {
          max = Math.floor(max_memory / choice_capacity);
        }
      }
  
      return max;
    }

    return;
  }

  /**
   * Return an numeric input field so the user can define the desired amount of memory banks
   */
  const canHaveMultiple = () => {
    if (props.canHaveMultiple) {
      const max = generateMax();
      let onChange = {};
  
      if (props.canHaveMultiple) {
        onChange = {
          onChange: (el: HTMLElement) => checkExceeding(max)
        }
      }
  
      return (
        <div className="mr-3 d-flex align-items-center w-100">
          <input
            type="number"
            className="form-control d-inline-block mr-1 w-auto"
            id="amount"
            min="1"
            max={max}
            defaultValue="1"
            {...onChange}
          />
          <label className="form-label mb-0" htmlFor="amount">
            {`x ${props.canHaveMultiple.choice_capacity}GB`}
          </label>
        </div>
      );
    }
  };

  /**
   * Generate a next link button
   */
  const nextLink = (link: string) => {
    return (
      <div className="next-step d-inline-block flex-shrink-0">
        <Link to={link} className="btn btn-primary">Next step</Link>
      </div>
    );
  }

  /**
   * Render view
   */
  return  (
    <div className="col col-12 col-md-8">
      <div className="box">
        { props.children.map((child: React.FC) => (
          child
        )) }
      </div>

      <div className="bottom mt-3 d-flex justify-content-between">
        { canHaveMultiple() }
        { props.nextLink ? nextLink(props.nextLink) : '' }
      </div>
    </div>
  );
}

export default DetailsCol;