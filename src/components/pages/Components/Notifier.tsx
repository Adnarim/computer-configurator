import { Link } from 'react-router-dom';

type Props = {
  notification: string,
  link: string,
};

const Notifier = (props: Props) => (
  <div className="row d-flex flex-grow-1 justify-content-center align-items-center flex-column">
    <div className="box rounded">
      <p>{props.notification}</p>
      <Link to={props.link} className="btn btn-primary">Go back</Link>
    </div>
  </div>
);

export default Notifier;