type Item = {
  id: string,
  name: string,
}

type Props = {
  items: Item[],
  name: string,
  onChange: (arg0: Item) => {},
  activeItem: string,
};

const ListCol = (props: Props) => (
  <div className="col col-12 col-md-4">
    <div className="box">
      { props.items.map((item, key) => (
        <div key={key} className="form-check">
          <input
            type="radio"
            name={props.name}
            id={item.id}
            onChange={() => props.onChange(item)}
            checked={item.id === props.activeItem}
            className="form-check-input"
          />

          <label htmlFor={item.id} className="form-check-label">
            {item.name}
          </label>
        </div>
      )) }
    </div>
  </div>
);

export default ListCol;