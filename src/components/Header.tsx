import { Nav, Navbar } from 'react-bootstrap';

type Props = {
  location: {
    pathname: string,
  }
}

/**
 * Navigation Items
 * @returns {Array}
 */
const renderNavItems = () => {
  const navItems = [
    {
      'name': 'Start',
      'link': '/'
    },
    {
      'name': '1. Motherboard',
      'link': '/motherboard',
    },
    {
      'name': '2. Memory',
      'link': '/memory',
    },
    {
      'name': '3. Video card',
      'link': '/video-card',
    },
    {
      'name': 'Finish',
      'link': '/finish',
    }
  ];

  return navItems.map((item, key) => (
    <Nav.Item as="li" key={key}>
      <Nav.Link href={item.link}>
        <span>{item.name}</span>
      </Nav.Link>
    </Nav.Item>
  ));
}

const Header = (props: Props) => (
  <header className="top-navigation secondary-bg">
    <Navbar sticky="top" variant="dark">
      {/*<Navbar.Toggle aria-controls="basic-navbar-nav" />*/}
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav
          className="justify-content-center nav"
          as="ul"
          activeKey={props.location.pathname}
          onSelect={() => {}}
        >
          { renderNavItems() }
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  </header>
);

export default Header;