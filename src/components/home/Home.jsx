import { Link } from 'react-router-dom';

const Home = () => (
  <section className="AppContent d-flex justify-content-center align-items-center flex-column">
    <h1>Welcome!</h1>
    <p>Let's configure your computer</p>
    <Link to="/motherboard" className="btn btn-primary">Step 1: Choose motherboard</Link>
  </section>
);

export default Home;