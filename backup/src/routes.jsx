import React from 'react';
import { Route, Switch } from 'react-router-dom';

import HomeComponent from './features/home/Home.jsx';
import MotherboardContainer from './features/pages/Containers/Motherboard.jsx';
import MemoryContainer from './features/pages/Containers/Memory.jsx';
import VideoCardContainer from './features/pages/Containers/VideoCard.jsx';
import FinishContainer from './features/pages/Containers/Finish.jsx';

export default () => (
  <Switch>
    <Route exact path="/" component={HomeComponent} name={'home'} />
    <Route exact path="/motherboard" component={MotherboardContainer} name={'motherboard'} />
    <Route exact path="/memory" component={MemoryContainer} name={'memory'} />
    <Route exact path="/video-card" component={VideoCardContainer} name={'video-card'} />
    <Route exact path="/finish" component={FinishContainer} name={'finish'} />
  </Switch>
);


