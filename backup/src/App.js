import React, { Component } from 'react';
import './App.css';
import './assets/scss/main.scss';
import { Nav, Navbar } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

class App extends Component {

  /**
   * Navigation Items
   * @returns {Array}
   */
  renderNavItems() {
    const navItems = [
      {
        'name': 'Start',
        'link': '/'
      },
      {
        'name': '1. Motherboard',
        'link': '/motherboard',
      },
      {
        'name': '2. Memory',
        'link': '/memory',
      },
      {
        'name': '3. Video card',
        'link': '/video-card',
      },
      {
        'name': 'Finish',
        'link': '/finish',
      }
    ];

    return navItems.map((item, key) => (
      <Nav.Item as="li" key={key}>
        <Nav.Link href={item.link}>
          <span>{item.name}</span>
        </Nav.Link>
      </Nav.Item>
    ));
  }

  /**
   * Render view
   * @returns {XML}
   */
  render() {
    return (
      <div className="App">
        <header className="top-navigation secondary-bg">
          <Navbar sticky="top" variant="custom">
            {/*<Navbar.Toggle aria-controls="basic-navbar-nav" />*/}
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav
                className="justify-content-center nav"
                as="ul"
                activeKey={this.props.location.pathname}
                onSelect={() => {}}
              >
                { this.renderNavItems() }
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </header>

        { this.props.children }
      </div>
    );
  }
}

export default withRouter(App);
