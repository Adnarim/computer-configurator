import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {

  render() {
    return (
      <section className="AppContent d-flex justify-content-center align-items-center flex-column">
        <h1>Welcome!</h1>
        <p>Let's configure your computer</p>
        <Link to="/motherboard" className="btn btn-primary">Step 1: Choose motherboard</Link>
      </section>
    );
  }
}

export default Home;