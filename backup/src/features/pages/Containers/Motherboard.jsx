import React, { Component } from 'react';
import data from '../data';
import Navigator from '../Components/Navigator';
import ListCol from '../Components/ListCol';
import DetailsCol from '../Components/DetailsCol';

class MotherboardContainer extends Component {

  /**
   * Constructor
   * @param props
   */
  constructor(props) {
    super(props);

    this.state = {
      choice: null,
    };

    this.data = data.motherboards;
    this.storageName = 'motherboard';
    this.navigator = {
      title: 'Select a motherboard',
      prev: '/',
      next: '/memory',
    }
  }

  /**
   * Define start data
   **/
  componentWillMount() {
    const storedChoice = localStorage.getItem(this.storageName);
    if (!storedChoice) {
      // define first motherboard as option into store and state
      this.changeChoice(this.data[0]);
    } else {
      // update state, we already have the data stored
      this.setState({ choice: JSON.parse(storedChoice) })
    }
  }

  /**
   * Update state and store with new choice
   **/
  changeChoice(choice) {
    this.setState({ choice });
    localStorage.setItem(this.storageName, JSON.stringify(choice));
  }

  /**
   * Render list of producs
   * @returns {XML}
   **/
  renderList() {
    return (
      <ListCol
        onChange={(item) => this.changeChoice(item)}
        items={this.data}
        activeItem={this.state.choice.id}
        name="boards"
      />
    );
  }

  /**
   * Render right column with product details
   * @returns {XML}
   */
  renderDetails() {
    const item = this.state.choice;

    return (
      <DetailsCol nextLink={this.navigator.next}>
        <div className="float-sm-right text-center">
          <img src={item.image} alt="product" />
        </div>
        <h2>{item.name}</h2>
        <div><strong>Socket</strong>: {item.socket}</div>
        <div><strong>Chipset</strong>: {item.chipset}</div>
        <div><strong>Formfactor</strong>: {item.formfactor}</div>
        <div><strong>Max memory</strong>: {item.memory_max}</div>
        <div><strong>Memory slots</strong>: {item.memory_slots}x {item.memory_slots_type}</div>
        {item.usbc_3_1 && <div><strong>USB-C 3.1</strong>: {item.usbc_3_1}</div>}
        {item.usb_3_1 && <div><strong>USB 3.1</strong>: {item.usb_3_1}</div>}
        {item.usb_3_0 && <div><strong>USB 3.0</strong>: {item.usb_3_0}</div>}
        {item.usb_2_0 && <div><strong>USB 2.0</strong>: {item.usb_2_0}</div>}
        {item.dvid && <div><strong>DVI-D</strong>: {item.dvid}</div>}
        {item.hdmi && <div><strong>HDMI</strong>: {item.hdmi}</div>}
        {item.mini_hdmi && <div><strong>Mini-HDMI</strong>: {item.mini_hdmi}</div>}
        <div><strong>Line-in</strong>: {item.linein}</div>
        <div><strong>Line-out</strong>: {item.lineout}</div>
        {item.rj45 && <div><strong>RJ-45</strong>: {item.rj45}</div>}
        {item.bluetooth && <div><strong>Bluetooth</strong>: {item.bluetooth}</div>}
        {item.wlan && <div><strong>WLAN</strong>: {item.wlan}</div>}
        <div><strong>Sata</strong>: {item.sata}</div>
      </DetailsCol>
    );
  }

  /**
   * Render view
   * @returns {XML}
   **/
  render() {
    return (
      <section className="AppContent d-flex justify-content-stretch align-items-stretch flex-column text-center">
        <Navigator
          title={this.navigator.title}
          // prev={this.navigator.prev}
          // next={this.navigator.next}
        />

        <div className="container-fluid d-flex flex-grow-1 align-items-stretch">
          <div className="row d-flex flex-grow-1">
            { this.renderList() }

            { this.renderDetails() }
          </div>
        </div>
      </section>
    );
  }
}

export default MotherboardContainer;