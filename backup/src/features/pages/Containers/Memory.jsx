import React, { Component } from 'react'
import data from '../data';
import Navigator from '../Components/Navigator';
import ListCol from '../Components/ListCol';
import DetailsCol from '../Components/DetailsCol';
import Notifier from '../Components/Notifier';

class MemoryContainer extends Component {

  /**
   * Constructor
   * @param props
   */
  constructor(props) {
    super(props);

    this.state = {
      choice: null,
    };

    this.data = data.memory_banks;
    this.storageName = 'memory';
    this.navigator = {
      title: 'Select a memory card',
      prev: '/motherboard',
      next: '/video-card',
    };
    this.hasPreviousStorage = false;
  }

  /**
   * Define start data and set default store data
   **/
  componentWillMount() {
    this.hasPreviousStorage = !!localStorage.getItem('motherboard');

    const storedChoice = localStorage.getItem(this.storageName);
    if (this.hasPreviousStorage) {
      if (!storedChoice) {
        // define first motherboard as option into store and state
          this.changeChoice(this.data[0]);
      } else {
        // update state, we already have the data stored
        this.setState({ choice: JSON.parse(storedChoice) });
      }
    }
  }

  /**
   * Set the amount field default value
   */
  componentDidMount() {
    const amountField = document.getElementById('amount');

    if (this.hasPreviousStorage) {
      const storedMemoryAmount = localStorage.getItem('memory_amount');

      if (storedMemoryAmount) {
        amountField.value = storedMemoryAmount;
      } else {
        localStorage.setItem('memory_amount', 1);
      }
    }
  }


  /**
   * Update state and store with new choice
   **/
  changeChoice(choice) {
    this.setState({ choice });
    localStorage.setItem(this.storageName, JSON.stringify(choice));
  }

  /**
   * Render list of producs
   * @returns {XML}
   **/
  renderList() {
    return (
      <ListCol
        onChange={(item) => this.changeChoice(item)}
        items={this.data}
        activeItem={this.state.choice.id}
        name="boards"
      />
    );
  }

  /**
   * Render right column with product details
   * @returns {XML}
   */
  renderDetails() {
    const item = this.state.choice;
    const motherboard = JSON.parse(localStorage.getItem('motherboard'));

    return (
      <DetailsCol
        nextLink={this.navigator.next}
        canHaveMultiple={{
          'max_memory': motherboard.memory_max,
          'slots': motherboard.memory_slots,
          'choice_capacity': item.capacity,
          'choice_pieces': item.pieces
        }}
      >
        <div className="float-sm-right text-center">
          <img src={item.image} alt="product" />
        </div>
        <h2>{item.name}</h2>
        <div><strong>Type</strong>: {item.type}</div>
        <div><strong>Capacity</strong>: {item.capacity}GB</div>
        <div><strong>Timings</strong>: {item.timings}</div>
        <div><strong>Pieces</strong>: {item.pieces}</div>
        <div><strong>Connector</strong>: {item.connector}</div>
        <div><strong>Voltage</strong>: {item.voltage}</div>
        <div><strong>Standard</strong>: {item.standard}</div>
        <div><strong>Physical Clock Speed</strong>: {item.physical_clock_speed}</div>
      </DetailsCol>
    );
  }

  /**
   * Render content
   * @returns {XML}
   */
  renderContent() {
    if (!this.hasPreviousStorage) {
      return (
        <Notifier notification="Please select a motherboard first" link={this.navigator.prev} />
      );
    }
    return (
      <div className="row d-flex flex-grow-1">
        { this.renderList() }

        { this.renderDetails() }
      </div>
    );
  }

  /**
   * Render view
   * @returns {XML}
   */
  render() {
    return (
      <section className="AppContent d-flex justify-content-stretch align-items-stretch flex-column text-center">
        <Navigator
          title={this.navigator.title}
          />

        <div className="container-fluid d-flex flex-grow-1 align-items-stretch">
          { this.renderContent() }
        </div>
      </section>
    );
  }
}

export default MemoryContainer;