import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Navigator from '../Components/Navigator';
import Notifier from '../Components/Notifier';

class FinishContainer extends Component {

  /**
   * Render content
   * @returns {XML}
   */
  renderContent() {
    const motherboard = JSON.parse(localStorage.getItem('motherboard'));
    const memory = JSON.parse(localStorage.getItem('memory'));
    const memory_amount = parseInt(localStorage.getItem('memory_amount'));
    const videocard = JSON.parse(localStorage.getItem('videocard'));

    if (!motherboard) {
      return (
        <Notifier notification="Please select a motherboard first" link="/motherboard" />
      );
    }
    if (!memory || !memory_amount) {
      return (
        <Notifier notification="Please select a memory bank first" link="/memory" />
      );
    }
    if (!videocard) {
      return (
        <Notifier notification="Please select a video card first" link="/video-card" />
      );
    }

    return (
      <div className="row d-flex flex-grow-1 justify-content-center">
        <div className="col-12 col-sm-10 col-md-8 col-lg-6">
          <div className="box text-left">
            <h2>Finished!</h2>
            <p>Here is an overview of your selected configuration:</p>
            <div className="overview">
              <div>
                <strong>Motherboard:</strong> {motherboard.name} <Link to="/motherboard">(change)</Link>
              </div>

              <div>
                <strong>Memory banks:</strong> {memory_amount} x {memory.name} <Link to="/memory">(change)</Link>
              </div>

              <div>
                <strong>Video card:</strong> {videocard.name} <Link to="/video-card">(change)</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * Render view
   * @returns {XML}
   */
  render() {
    return (
      <section className="AppContent d-flex justify-content-stretch align-items-stretch flex-column text-center">
        <Navigator
          title="Finished!"
        />

        <div className="container-fluid d-flex flex-grow-1 align-items-stretch">
          { this.renderContent() }
        </div>
      </section>
    );
  }
}

export default FinishContainer;