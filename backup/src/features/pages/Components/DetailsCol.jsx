import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class DetailsCol extends Component {

  /**
   * Store amount field as reusable variable
   */
  componentDidMount() {
    if (this.props.canHaveMultiple) {
      this.amount = document.getElementById('amount');
    }
  }

  /**
   * If choice changes, recheck if current amount is not exceeding the new max
   * @param state
   */
  componentWillUpdate(state) {
    if (state.canHaveMultiple) {
      this.checkExceeding(this.generateMax(state.canHaveMultiple));
    }
  }

  /**
   * Check if the current amount is not exceeding the max
   * and store the chosen amount in the store
   * @param max
   */
  checkExceeding(max) {
    const value = parseInt(this.amount.value);

    if(value > max) {
      this.amount.value = max;
    }

    if (this.props.canHaveMultiple) {
      localStorage.setItem('memory_amount', this.amount.value);
    }
  }

  /**
   * Check what can be the max based on memory bank and motherboard specs
   * @param settings
   * @returns {number}
   */
  generateMax(settings) {
    const slots = parseInt(settings.slots);
    const max_memory = parseInt(settings.max_memory);
    const choice_capacity = parseInt(settings.choice_capacity);
    const choice_pieces = parseInt(settings.choice_pieces);

    let max = 1;
    if (slots >= choice_pieces) {
      if (max_memory % choice_capacity === 0) {
        max = max_memory / choice_capacity;
      } else {
        max = Math.floor(max_memory / choice_capacity);
      }
    }

    return max;
  }

  /**
   * Return an numeric input field so the user can define the desired amount of memory banks
   * @param settings
   * @returns {XML}
   */
  canHaveMultiple(settings) {
    const max = this.generateMax(settings);
    let onChange = {};

    if (this.props.canHaveMultiple) {
      onChange = {
        onChange: (el) => this.checkExceeding(max)
      }
    }

    return (
      <div className="mr-3 d-flex align-items-center w-100">
        <input
          type="number"
          className="form-control d-inline-block mr-1 w-auto"
          id="amount"
          min="1"
          max={max}
          defaultValue="1"
          {...onChange}
        />
        <label className="form-label mb-0" htmlFor="amount">
          {`x ${settings.choice_capacity}GB`}
        </label>
      </div>
    );
  };

  /**
   * Generate a next link button
   * @param link
   * @returns {XML}
   */
  nextLink(link) {
    return (
      <div className="next-step d-inline-block flex-shrink-0">
        <Link to={link} className="btn btn-primary">Next step</Link>
      </div>
    );
  }

  /**
   * Render view
   * @returns {XML}
   */
  render() {
    return  (
      <div className="col col-12 col-md-8 text-left">
        <div className="box">
          { this.props.children.map((child) => (
            child
          )) }
        </div>

        <div className="bottom mt-3 d-flex justify-content-between">
          { this.props.canHaveMultiple ? this.canHaveMultiple(this.props.canHaveMultiple) : false }
          { this.props.nextLink ? this.nextLink(this.props.nextLink) : '' }
        </div>
      </div>
    );
  }
}

export default DetailsCol;