import React from 'react';

const Navigator = (props) => (
  <div className="navigator d-flex primary-bg mb-3 justify-content-center align-items-center p-2">
    <h1 className="mb-0">{props.title}</h1>
  </div>
);

export default Navigator;