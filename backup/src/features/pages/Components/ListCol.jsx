import React from 'react';

const ListCol = (props) => (
  <div className="col col-12 col-md-4 text-left">
    <div className="box">
      { props.items.map((item, key) => (
        <div key={key} className="form-check">
          <input
            type="radio"
            name={props.name}
            id={item.id}
            onChange={() => props.onChange(item)}
            checked={item.id === props.activeItem ? 'checked' : false}
            className="form-check-input"
          />

          <label htmlFor={item.id} className="form-check-label">
            {item.name}
          </label>
        </div>
      )) }
    </div>
  </div>
);

export default ListCol;