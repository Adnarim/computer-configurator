import React from 'react';
import { Link } from 'react-router-dom';

const Notifier = (props) => (
  <div className="row d-flex flex-grow-1 justify-content-center align-items-center flex-column">
    <div className="box rounded">
      <p>{props.notification}</p>
      <Link to={props.link} className="btn btn-primary">Go back</Link>
    </div>
  </div>
);

export default Notifier;